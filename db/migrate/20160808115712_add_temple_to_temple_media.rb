class AddTempleToTempleMedia < ActiveRecord::Migration[5.0]
  def change
    add_reference :temple_media, :temple, foreign_key: true
  end
end
