class CreateTempleMedia < ActiveRecord::Migration[5.0]
  def change
    create_table :temple_media do |t|
      t.string :media_url
      t.string :media_type

      t.timestamps
    end
  end
end
