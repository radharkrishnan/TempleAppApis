class AddUserToTemple < ActiveRecord::Migration[5.0]
  def change
    add_reference :temples, :user, foreign_key: true
  end
end
