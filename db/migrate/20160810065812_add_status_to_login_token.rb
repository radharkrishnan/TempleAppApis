class AddStatusToLoginToken < ActiveRecord::Migration[5.0]
  def change
    add_column :login_tokens, :status, :string
  end
end
