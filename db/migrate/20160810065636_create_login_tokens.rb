class CreateLoginTokens < ActiveRecord::Migration[5.0]
  def change
    create_table :login_tokens do |t|
      t.string :device_token

      t.timestamps
    end
  end
end
