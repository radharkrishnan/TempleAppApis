class CreateTemples < ActiveRecord::Migration[5.0]
  def change
    create_table :temples do |t|
      t.string :name
      t.text :short_description
      t.text :full_description
      t.string :lat_long
      t.string :main_image

      t.timestamps
    end
  end
end
