class UsersController < ApplicationController
  def index
  	@users = User.all

  	@temples = User.first.temples

    render json: @users
  end
end
