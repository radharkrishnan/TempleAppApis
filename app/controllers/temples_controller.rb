class TemplesController < ApplicationController
	require 'geokit'
	include Geokit::Geocoders

	def device_token_error 
	 	'{ "status": 0,
	       "message": "device_token not found" }'
	end
  	def index
  		latitude = params[:latitude]
  		longitude = params[:longitude]
  		#login_token = LoginToken.where("device_token = ?", params[:device_token])

  		#if(!login_token.empty?)
		  	eachdata = Hash.new
		  	fulldata = Array.new
		  	
		  	temples = Temple.all

		  	temples.each do |temple|
		     	eachdata = temple_details(temple)
		     	if(params[:latitude].present? && params[:longitude].present?)
		     		current_location = Geokit::LatLng.new(latitude,longitude)
					destination = eachdata['temple']['lat_long']
					distance_btw = current_location.distance_to(destination) #in miles
					eachdata['temple_distance'] = distance_btw / 0.62137 #in kms  
					eachdata['temple_distance'] = '%.2f' % eachdata['temple_distance']+" kms" #round off to 2 decimals
				else
				   	eachdata['temple_distance'] = ""  	
		     	end

		     	fulldata.push(eachdata)
		    end
		    #sort the data based on distance - nearest first
		    fulldata = fulldata.sort { |a, b| b['temple_distance'] <=> a['temple_distance'] }.reverse

		    response = '{ "status": 1,
			    		  "message": "success fetching temple list",
			    		  "response": '+fulldata.to_json()+' }'
		    render json: response
		#else
		#	render json: device_token_error
		#end
  	end
  	def show
  		latitude = params[:latitude]
  		longitude = params[:longitude]
  		location_info = Array.new
  		#login_token = LoginToken.where("device_token = ?", params[:device_token])

  		#if(!login_token.empty?)
		  	temple = Temple.find(params[:id])
		  	temple_details = Hash.new

		  	temple_details = temple_details(temple)

		  	if(params[:latitude].present? && params[:longitude].present?)
	     		current_location = Geokit::LatLng.new(latitude,longitude)
				destination = temple_details['temple']['lat_long']
				distance_btw = current_location.distance_to(destination) #in miles
				temple_details['temple_distance'] = distance_btw / 0.62137 #in kms  
				temple_details['temple_distance'] = '%.2f' % temple_details['temple_distance']+" kms" #round off to 2 decimals
			else
			   	temple_details['temple_distance'] = ""  	
	     	end

  	     	# reference for location address finding: http://geokit.rubyforge.org/

			location_info						= GoogleGeocoder.reverse_geocode(temple_details['temple']['lat_long'])

			temple_details['temple_address']	= location_info.full_address

			temple_details['temple_address'].gsub! ', ', "\n"

			response = '{ "status": 1,
			    		  "message": "success fetching temple list",
			    		  "response": '+temple_details.to_json()+' }'
		  	render json: response
		#else
		#	render json: device_token_error
		#end
  	end
  	private 
  	def temple_details(temple)
  		temple_details = Hash.new

	  	tmedia = TempleMedium.where("temple_id = ?", temple)
     	tmedias = tmedia if tmedia

     	temple_details['temple'] = temple
     	temple_details['temple_media'] = tmedias

     	return temple_details
  	end
end
