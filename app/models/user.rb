class User < ApplicationRecord
	has_many :temples
	enum user_type: [:admin, :editor, :user ]
end
