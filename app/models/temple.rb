class Temple < ApplicationRecord
	belongs_to :user
	has_many :temple_mediums
end
