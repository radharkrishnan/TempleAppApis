-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 19, 2016 at 02:41 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.6.14-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `temple_app_apis`
--

-- --------------------------------------------------------

--
-- Table structure for table `ar_internal_metadata`
--

CREATE TABLE IF NOT EXISTS `ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_internal_metadata`
--

INSERT INTO `ar_internal_metadata` (`key`, `value`, `created_at`, `updated_at`) VALUES
('environment', 'development', '2016-08-08 11:57:46', '2016-08-08 11:57:46');

-- --------------------------------------------------------

--
-- Table structure for table `login_tokens`
--

CREATE TABLE IF NOT EXISTS `login_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `login_tokens`
--

INSERT INTO `login_tokens` (`id`, `device_token`, `created_at`, `updated_at`, `status`) VALUES
(1, 'abcdefghij', '2016-08-10 12:34:03', '2016-08-10 12:34:03', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `schema_migrations`
--

CREATE TABLE IF NOT EXISTS `schema_migrations` (
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schema_migrations`
--

INSERT INTO `schema_migrations` (`version`) VALUES
('20160808115511'),
('20160808115528'),
('20160808115647'),
('20160808115658'),
('20160808115712'),
('20160810065636'),
('20160810065812');

-- --------------------------------------------------------

--
-- Table structure for table `temples`
--

CREATE TABLE IF NOT EXISTS `temples` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `short_description` text,
  `full_description` text,
  `lat_long` varchar(255) DEFAULT NULL,
  `main_image` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_temples_on_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `temples`
--

INSERT INTO `temples` (`id`, `name`, `short_description`, `full_description`, `lat_long`, `main_image`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 'Ettumanoor', 'This is the shortest description about this temple', 'This is the full description in detail about this temple, which can be so long or short.', '9.673141,76.5463677', 'image.jpg', '2016-08-08 18:04:03', '2016-08-08 18:04:03', 1),
(2, 'Vaikom', 'Shortest description for temple two', 'The Full description for temple two bla bla bla', '9.7499623,76.3937263', 'images2.jpg', '2016-08-08 18:41:03', '2016-08-08 18:41:03', 1),
(3, 'Thrissur', 'Test temple 3 at thrissur', 'Test temple 3 at thrissur full description', '10.5243325,76.2122708', 'Panda-Single-1000x600.jpg', '2016-08-19 00:00:00', '2016-08-19 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `temple_media`
--

CREATE TABLE IF NOT EXISTS `temple_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_url` varchar(255) DEFAULT NULL,
  `media_type` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `temple_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_temple_media_on_temple_id` (`temple_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `temple_media`
--

INSERT INTO `temple_media` (`id`, `media_url`, `media_type`, `created_at`, `updated_at`, `temple_id`) VALUES
(1, 'images/testimg1.png', 'image', '2016-08-09 11:04:03', '2016-08-09 11:04:03', 1),
(2, 'images/testimg2.png', 'image', '2016-08-09 11:04:03', '2016-08-09 11:04:03', 1),
(3, 'images/secondtempleimg.png', 'image', '2016-08-09 11:04:03', '2016-08-09 11:04:03', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `first_name`, `last_name`, `password`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'radha@alignminds.com', 'Radha', 'R Krishnan', '9863b5825539149420dd2829e58eae40', 'admin', '2016-08-08 18:04:03', '2016-08-08 18:04:03');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `temples`
--
ALTER TABLE `temples`
  ADD CONSTRAINT `fk_rails_340eafbe96` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `temple_media`
--
ALTER TABLE `temple_media`
  ADD CONSTRAINT `fk_rails_f2a5137ace` FOREIGN KEY (`temple_id`) REFERENCES `temples` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
